import 'package:flutter/material.dart';
import 'package:software_engineering_project/pages/profile_page.dart';
import 'package:software_engineering_project/pages/search_page.dart';
import 'package:software_engineering_project/util/font_weights.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
    BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile'))
  ];
  List<Widget> pages;
  int navigationIndex;

  @override
  void initState() {
    navigationIndex = 0;
    pages = [SearchPage(), ProfilePage()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(navigationIndex == 0 ? 'Search' : 'Profile'),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: items,
        onTap: (i) => setState(() => navigationIndex = i),
        currentIndex: navigationIndex,
      ),
      body: IndexedStack(
        children: pages,
        index: navigationIndex,
      ),
    );
  }
}
