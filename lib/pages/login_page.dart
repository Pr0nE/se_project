import 'package:flutter/material.dart';
import 'package:software_engineering_project/pages/home_page.dart';
import 'package:software_engineering_project/repo/users.dart';
import 'package:software_engineering_project/util/snack_bar.dart';
import 'package:software_engineering_project/widgets/custom_textfield.dart';
import 'package:software_engineering_project/widgets/main_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({
    Key key,
  }) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _emailController;
  TextEditingController _passController;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //  resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: IgnorePointer(
              ignoring: true,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                    height: 200,
                    child: Image.asset(
                      'assets/images/login.png',
                      fit: BoxFit.contain,
                      width: double.infinity,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 20),
            child: Align(
              alignment: Alignment.topCenter,
              child: TweenAnimationBuilder(
                tween: Tween<double>(begin: 0, end: 0),
                duration: Duration(milliseconds: 350),
                curve: Curves.easeOut,
                builder: (c, val, w) =>
                    Transform.translate(offset: Offset(0, val), child: w),
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(
                            height: 30,
                          ),
                          CustomTextField(
                            controller: _emailController,
                            placeholder: 'Email',
                            type: TextInputType.emailAddress,
                          ),
                          SizedBox(
                            height: 11,
                          ),
                          CustomTextField(
                              controller: _passController,
                              placeholder: 'Password',
                              obscureText: true),
                          SizedBox(
                            height: 11,
                          ),
                          Builder(
                            builder: (c)=>
                            MainButton(
                              onPressed: () => _loginPressed(c),
                              title: 'Login',
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(9),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 25,
                            offset: Offset(3, 3),
                            color: Colors.black.withOpacity(.11))
                      ]),
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }

  bool isValidUser() {
    return _emailController.text == UserRepo.mainUser.email &&
        _passController.text == UserRepo.mainUser.password;
  }

  _loginPressed(BuildContext c) async {
    if (!isValidUser()) {
      showSnack(context: c, text: 'Wrong username or password');
      return;
    }
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (c) => HomePage()));
  }
}
