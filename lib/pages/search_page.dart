import 'package:flutter/material.dart';
import 'package:software_engineering_project/model/post.dart';
import 'package:software_engineering_project/repo/posts.dart';
import 'package:software_engineering_project/util/font_weights.dart';
import 'package:software_engineering_project/widgets/post_cell.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<Post> filteredPosts;
  List<Post> get allPosts => PostRepo.posts;

  @override
  void initState() {
    filteredPosts = List.of(allPosts);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 38),
            child: TextField(
              onChanged: _onTextChange,
              textInputAction: TextInputAction.search,
              style: TextStyle(fontSize: 17),
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide:
                        BorderSide(color: Colors.deepPurple, width: 2.5)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(color: Colors.black45, width: 2.5)),
                hintText: 'Search for titles or hashtags (ex: #hashtag)',
                hintStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: Weight.regular,
                  color: Color(0xffB5B5B5),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(top: 30, left: 14, right: 14),
            child: ListView.separated(
                separatorBuilder: (c, i) => SizedBox(
                      height: 20,
                    ),
                itemCount: filteredPosts.length,
                itemBuilder: (c, i) => PostCell(
                      post: filteredPosts[i],
                    )),
          ))
        ],
      ),
    );
  }

  _onTextChange(String str) {
    final results = allPosts.where((p) => _filterPost(p, str)).toList();
    setState(() {
      filteredPosts = results;
    });
  }

  bool _filterPost(Post p, String searched) {
    final firstCond = p.title.toLowerCase().contains(searched.toLowerCase());
    final secCond = searched.startsWith('#') &&
        p.tags.any((t) =>
            t.toLowerCase().contains(searched.substring(1).toLowerCase()));
    return firstCond || secCond;
  }
}
