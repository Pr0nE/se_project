import 'package:flutter/material.dart';
import 'package:software_engineering_project/model/user.dart';
import 'package:software_engineering_project/pages/login_page.dart';
import 'package:software_engineering_project/repo/users.dart';
import 'package:software_engineering_project/util/font_weights.dart';
import 'package:software_engineering_project/util/snack_bar.dart';
import 'package:software_engineering_project/widgets/custom_textfield.dart';
import 'package:software_engineering_project/widgets/main_button.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  User get user => UserRepo.mainUser;
  TextEditingController _passController;
  TextEditingController _emailController;

  @override
  void initState() {
    _passController = TextEditingController(text: user.password);
    _emailController = TextEditingController(text: user.email);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Posts: ',
                      style:
                          TextStyle(fontWeight: Weight.semiBold, fontSize: 15),
                    ),
                    Text(
                      user.postCount.toString(),
                      style: TextStyle(
                          fontWeight: Weight.semiBold,
                          fontSize: 14,
                          color: Colors.black54),
                    ),
                  ],
                ),
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.asset(
                      'assets/images/profile.png',
                      height: 120,
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Likes: ',
                      style:
                          TextStyle(fontWeight: Weight.semiBold, fontSize: 15),
                    ),
                    Text(
                      user.likesCount.toString(),
                      style: TextStyle(
                          fontWeight: Weight.semiBold,
                          fontSize: 14,
                          color: Colors.black54),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            user.fullName,
            style: TextStyle(fontWeight: Weight.black, fontSize: 23),
          ),
          SizedBox(
            height: 70,
          ),
          Row(
            children: [
              SizedBox(
                width: 90,
                child: Text(
                  'Email:',
                  style: TextStyle(fontWeight: Weight.bold),
                ),
              ),
              Expanded(
                child: CustomTextField(
                  controller: _emailController,
                  placeholder: 'Email',
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              SizedBox(
                width: 90,
                child: Text(
                  'Password:',
                  style: TextStyle(fontWeight: Weight.bold),
                ),
              ),
              Expanded(
                child: CustomTextField(
                  controller: _passController,
                  placeholder: 'Password',
                  obscureText: true,
                ),
              ),
            ],
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              children: [
                MainButton(
                    onPressed: () {
                      user.email = _emailController.text;
                      user.password = _passController.text;

                      showSnack(context: context, text: 'Saved!');
                    },
                    title: 'Save Changes'),
                SizedBox(
                  height: 3,
                ),
                MainButton(
                  onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (c) => LoginPage()),
                      (Route<dynamic> route) => false),
                  title: 'Logout',
                  color: Colors.redAccent,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
