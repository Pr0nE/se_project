import 'package:software_engineering_project/model/post.dart';

class PostRepo {
  static final posts = [
    Post(
        title: 'Dr Disrespect officially returns to streaming',
        createTime: DateTime.now(),
        description:
            'Guy “Dr Disrespect” Beahm has returned to streaming, this time on YouTube',
        upVotes: 83,
        tags: ['game', 'entertainment', 'ESFP']),
    Post(
        title:
            'SpaceX, ULA are the big winners for US national security launches',
        createTime: DateTime.now().subtract(Duration(days: 1)),
        description:
            'The US Department of Defense has selected its two primary rocket companies for getting satellites into orbit in the years ahead',
        upVotes: 30,
        tags: ['space', 'science', 'ISTJ']),
    Post(
        title: 'Trump bans WeChat',
        createTime: DateTime.now().subtract(Duration(days: 2)),
        description:
            'Trump’s WeChat ban could touch everything from Spotify to League of Legends',
        upVotes: 90,
        tags: ['game', 'politic', 'ENTP']),
    Post(
        title: 'Microsoft condemns Apple’s App Store policies',
        createTime: DateTime.now(),
        description:
            'Microsoft is now rebuking Apple over its stringent developer restrictions and its stance on cloud gaming apps',
        upVotes: 33,
        tags: ['tech', 'entertainment', 'ESFP']),
    Post(
        title: 'A weekly digest of the latest COVID-19 research',
        createTime: DateTime.now().subtract(Duration(days: 1)),
        description:
            'Antibodies remain elusive in the US, and the Oxford vaccine makes progress',
        upVotes: 15,
        tags: ['research', 'science', 'ISTJ']),
    Post(
        title: 'Kris Kobach projected to lose Kansas primary',
        createTime: DateTime.now().subtract(Duration(days: 1)),
        description:
            'Decision Desk is projecting that Rep. Roger Marshall will win the Republican primary for Senate in Kansas',
        upVotes: 5,
        tags: ['policy', 'ENTP']),
  ];
}
