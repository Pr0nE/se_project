import 'package:software_engineering_project/model/user.dart';

class UserRepo {
  static final mainUser = User(
      email: 'test@test.com',
      likesCount: 20,
      fullName: 'Group C',
      postCount: 11,
      password: 'test',
      birthday: DateTime.now().subtract(Duration(days: 365 * 22)));
}
