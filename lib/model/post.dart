
import 'package:flutter/foundation.dart';

class Post {
  final String title;
  final int upVotes;
  final List<String> tags;
  final String description;
  final DateTime createTime;

   Post(
      {@required this.upVotes,
      @required this.tags,
      @required this.description,
      @required this.createTime,
      @required this.title});
}
