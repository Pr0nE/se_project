import 'package:flutter/foundation.dart';

class User {
  final String fullName;
   String email;
   String password;
  final int postCount;
  final int likesCount;
  final DateTime birthday;

  User({
    @required this.fullName,
    @required this.email,
    @required this.password,
    @required this.likesCount,
    @required this.postCount,
    @required this.birthday,
  });
}
