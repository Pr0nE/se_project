import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

showSnack({@required BuildContext context, String text}) {
  final snackBar = SnackBar(content: Text(text));
  Scaffold.of(context).showSnackBar(snackBar);
}
