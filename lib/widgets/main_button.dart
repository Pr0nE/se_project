import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final Function onPressed;
  final Function onLongPressed;
  final ButtonType type;
  final String title;
  final Color color;
  final TextStyle titleStyle;
  final EdgeInsets padding;

  const MainButton(
      {@required this.onPressed,
      this.type = ButtonType.filled,
      @required this.title,
      this.color,
      this.titleStyle,
      this.padding,
      this.onLongPressed});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 0,
      onLongPress: onLongPressed,
      splashColor: Colors.transparent,
      disabledColor: Colors.grey,
      minWidth: double.infinity,
      padding: padding ?? EdgeInsets.symmetric(vertical: 13, horizontal: 20),
      onPressed: onPressed,
      child: Text(
        title,
        style: titleStyle ??
            Theme.of(context).textTheme.button.copyWith(
                color: type == ButtonType.filled
                    ? Colors.white
                    : Theme.of(context).primaryColor),
      ),
      color: type == ButtonType.filled
          ? color ?? Theme.of(context).primaryColor
          : Colors.white,
      shape: StadiumBorder(
        side: BorderSide(
          width: 1,
          color: type == ButtonType.filled
              ? Colors.transparent
              : color ?? Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}

enum ButtonType { filled, outlined }
