import 'package:flutter/material.dart';
import 'package:software_engineering_project/util/font_weights.dart';

class CustomTextField extends StatefulWidget {
  final String placeholder;
  final Widget prefix;
  final bool obscureText;
  final TextInputType type;
  final TextEditingController controller;

  const CustomTextField(
      {@required this.placeholder,
      this.obscureText = false,
      @required this.controller,
      this.type,
      this.prefix});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isObscureText;
  TextStyle get textStyle => TextStyle(
      fontSize: 14,
      fontWeight: Weight.regular,
      color: Theme.of(context).primaryColor);
  @override
  void initState() {
    isObscureText = widget.obscureText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        TextField(
          controller: widget.controller,
          keyboardType: widget.type,
          obscureText: widget.obscureText ? isObscureText : false,
          style: textStyle,
          decoration: InputDecoration(
            prefix: widget.prefix,
            hintStyle: textStyle.copyWith(
                color: Color(
                  0xffB5B5B5,
                ),
                fontSize: 11),
            contentPadding: EdgeInsets.symmetric(horizontal: 16),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: Color(0xffE0E0E0), width: 1.0),
            ),
            hintText: widget.placeholder,
          ),
        ),
        if (widget.obscureText)
          Align(
            alignment: Alignment.centerRight,
            child: IconButton(
              onPressed: () {
                setState(() {
                  isObscureText = !isObscureText;
                });
              },
              icon: isObscureText
                  ? Icon(
                      Icons.visibility_off,
                      color: Colors.grey,
                    )
                  : Icon(
                      Icons.visibility,
                      color: Colors.pink,
                    ),
            ),
          )
      ],
    );
  }
}
