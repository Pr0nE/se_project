import 'package:flutter/material.dart';
import 'package:software_engineering_project/model/post.dart';
import 'package:software_engineering_project/util/font_weights.dart';

class PostCell extends StatelessWidget {
  final Post post;

  const PostCell({@required this.post});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),

        color: Colors.grey.withOpacity(0.25),
        // boxShadow: [
        //   BoxShadow(
        //       color: Colors.black.withOpacity(0.16),
        //       spreadRadius: 2,
        //       offset: Offset(0, 6))
        // ]
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                post.title,
                style: TextStyle(fontWeight: Weight.semiBold, fontSize: 17),
                overflow: TextOverflow.ellipsis,
              )),
              SizedBox(
                width: 20,
              ),
              Row(
                children: [
                  Icon(
                    Icons.favorite,
                    color: Colors.deepPurple,
                    size: 18,
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Text(post.upVotes.toString())
                ],
              )
            ],
          ),
          SizedBox(
            height: 6,
          ),
          Text(post.description),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text(
                post.createTime.year.toString() +
                    '/' +
                    post.createTime.month.toString() +
                    '/' +
                    post.createTime.day.toString(),
                style: TextStyle(fontWeight: Weight.semiBold,fontSize: 12,color: Colors.black45),
              ),
              Spacer(),
              Text(
                post.tags.map((e) => '#' + e).join(' '),
                style: TextStyle(fontWeight: Weight.bold,fontSize: 12.5),
              ),
            ],
          )
        ],
      ),
    );
  }
}
